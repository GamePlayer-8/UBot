# Discord-bot
A calendar bot, for sending embeds on specific channels. Can be configured quickly. Works on multiple servers.

## USAGE
### Starting on server instance
 - Firstly clone git repository via command `git clone <repository url>` and enter to the directory via `cd <created directory>`.
 - Create file `token` in the same directory as `bot.py` file and put there the Discord Bot token.
 - Optionally open `bot.py` in the text editor and edit `HOSTUSER` parameter (or any other near it). Variables names says what they're configuring.
 - Use command `python3 bot.py` to launch the bot.
 - Enjoy.

### Getting help
After the bot joined your Discord Server you can just type `u!help` for getting all available commands.
