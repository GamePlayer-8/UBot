"""
####################################
# Copyright C 2021-2022 GamePlayer #
#     AERO Kianit License 1.0      #
####################################
"""

import os
import sys
import datetime
import asyncio
import discord
from discord.ext import commands
try:
    from PrintMe import welcome
except Exception:
    pass

TIMEZONE = "1"
HOSTUSER = "GamePlayer#7595"
TOKEN = ""
WORKPATH = "."
PREFIX = "u!"


def get_full_datetime():
    """ Define actual datetime """
    datetime_object = datetime.datetime.now()
    delta_time = datetime_object.hour + int(TIMEZONE)
    if delta_time > 23:
        delta_time = delta_time - 24
    datetime_object = datetime_object.replace(hour=delta_time)
    return "[" + str(datetime_object) + "]: "


# Emoji
EMOJI_FAILED = "⛔"
EMOJI_SUCCESS = "✅"
EMOJI_OK = "👌"
EMOJI_QUESTION = "\N{QUESTION MARK}"
EMOJI_SAVE = "🟢"


def checkf(path):
    """ Check file/path existance """
    return os.path.exists(path)


def mkdir(path):
    """ create folder """
    os.makedirs(path)


def rmfile(path):
    """ remove file """
    if os.path.exists(path):
        os.remove(path)


def rmdir(path):
    """ remove folder """
    dirs = os.listdir(path)
    for obj in dirs:
        if os.path.isdir(path + "/" + obj):
            rmdir(path + "/" + obj)
        else:
            rmfile(path + "/" + obj)
    os.rmdir(path)


def mkfile(path, data):
    """ create file with TXT content """
    with open(path, 'w', encoding='UTF-8') as file:
        file.write(str(data))


def rdfile(path):
    """ Read TXT file """
    lines = []
    with open(path, 'r', encoding='UTF-8') as file:
        lines = file.readlines()

    data = ""
    ticker = 0
    for _line in lines:
        ticker = ticker + 1
        data = data + _line
        if not ticker == len(lines):
            data = data + "\n"
    return data


def array_has_index(index, array):
    """ Check if array has index """
    return len(array) - 1 >= index


def array_has(base_object, array):
    """ Check if array has object """
    for _object in array:
        if _object == base_object:
            return True
    return False


def array_find(base_object, array):
    """ Find object in array """
    pos = 0
    for _object in array:
        if _object == base_object:
            return pos
        pos += 1
    return -1


def decontruct_string(_string):
    """ Debuild string to words in array """
    string = ""
    args = []
    tick = 0
    for _i in _string:
        if not _string[tick] == " ":
            string = string + _string[tick]
        else:
            args += [string]
            string = ""
        tick += 1
    args += [string]
    return args


def string_has(letter, string):
    """ Check if letter exist in string """
    for _letter in string:
        if _letter == letter:
            return True
    return False


if checkf(WORKPATH + "/token"):
    print(get_full_datetime() + "Overriding token from file.")
    TOKEN = rdfile(WORKPATH + "/token")

if TOKEN == "":
    print(get_full_datetime() + "token is empty, exiting...")
    sys.exit()

if checkf(WORKPATH + "/prefix"):
    print(get_full_datetime() + "Overriding prefix from file.")
    PREFIX = rdfile(WORKPATH + "/prefix")

if not checkf(WORKPATH + "/configs"):
    mkdir(WORKPATH + "/configs")

# Get/Set server config


def read_config(guild_id, time):
    """ Read calendar data """
    if checkf(WORKPATH + "/configs/" + str(guild_id)):
        if checkf(WORKPATH + "/configs/" + str(guild_id) + "/" + str(time)):
            return [
                rdfile(
                    WORKPATH +
                    "/configs/" +
                    str(guild_id) +
                    "/" +
                    str(time) +
                    "/title"),
                rdfile(
                    WORKPATH +
                    "/configs/" +
                    str(guild_id) +
                    "/" +
                    str(time) +
                    "/desc"),
                rdfile(
                    WORKPATH +
                    "/configs/" +
                    str(guild_id) +
                    "/" +
                    str(time) +
                    "/channel")]
        return []
    return []


def write_config(guild_id, time, title, desc, channel):
    """ Create new calendar data """
    if not checkf(WORKPATH + "/configs/" + str(guild_id)):
        mkdir(WORKPATH + "/configs/" + str(guild_id))
    if not checkf(WORKPATH + "/configs/" + str(guild_id) + "/" + str(time)):
        mkdir(WORKPATH + "/configs/" + str(guild_id) + "/" + str(time))
    mkfile(
        WORKPATH +
        "/configs/" +
        str(guild_id) +
        "/" +
        str(time) +
        "/title",
        title)
    mkfile(
        WORKPATH +
        "/configs/" +
        str(guild_id) +
        "/" +
        str(time) +
        "/desc",
        desc)
    mkfile(
        WORKPATH +
        "/configs/" +
        str(guild_id) +
        "/" +
        str(time) +
        "/channel",
        channel)


def remove_config(guild_id, time):
    """ Remove calendar data """
    if checkf(WORKPATH + "/configs/" + str(guild_id) + "/" + str(time)):
        rmdir(WORKPATH + "/configs/" + str(guild_id) + "/" + str(time))


async def show_calendar(message, args):
    """ Show calendar """
    if args[0] == "calendar":

        start = 0

        if len(args) > 1:
            if args[1].isnumeric():
                start = int(args[1]) - 1

        if len(os.listdir(
                    WORKPATH + "/configs/" + str(message.guild.id)
                )) == 0:
            await message.channel.send(
                embed=discord.Embed(
                    title='Calendar',
                    description='**Empty.**',
                    color=0x0099ff
                )
            )
            await message.add_reaction(EMOJI_OK)
            return True

        if len(os.listdir(
                    WORKPATH + "/configs/" + str(message.guild.id)
                )) <= start:
            await message.channel.send("**Out of range.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if checkf(WORKPATH + "/configs/" + str(message.guild.id)):

            real_tick = 0
            tick = 0
            text = ""
            for event in os.listdir(
                    WORKPATH + "/configs/" + str(message.guild.id)
                ):
                if real_tick >= start:
                    config = read_config(message.guild.id, event)
                    if checkf(WORKPATH + "/configs/" +
                            str(message.guild.id) + "/" + event + "/lock"):
                        text += "**" + str(start + tick + 1) + ".** `" + event + "` ~~**" + \
                            config[0] + " - " + config[2] + "**~~\n"
                    else:
                        text += "**" + str(start + tick + 1) + ".** `" + event + "` **" + \
                            config[0] + " - " + config[2] + "**\n"

                    tick += 1
                    if tick > 10:
                        break
                real_tick += 1

            pages = len(os.listdir(
                    WORKPATH + "/configs/" + str(message.guild.id)
                    ))/10

            if pages - round(pages) > 0:
                pages += 1

            pages = round(pages)

            await message.channel.send(
                embed=discord.Embed(
                    title='Calendar',
                    description=text + \
                    "\nPages: `" + str(pages) + \
                    "`.\nDisplaying `" + str(start + 1) + "/" + str(start + 10) + "`.",
                    color=0x0099ff
                )
            )
            await message.add_reaction(EMOJI_OK)
            return True
        await message.channel.send(
            embed=discord.Embed(
                title='Calendar',
                description='**Empty.**',
                color=0x0099ff
            )
        )
        await message.add_reaction(EMOJI_OK)
        return True


async def add(message, args):
    """ Add new rule to the calendar """
    if args[0] == "add":
        if not len(args) > 4:
            await message.channel.send("""**Missing arguments.\n
                Usage: `""" + PREFIX + "add <Day>:<Hour>:<Minute> <Title> <Desc> <Channel>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        time = args[1].split(":")
        if not len(time) == 3:
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "add <Day>:<Hour>:<Minute> <Title> <Desc> <Channel>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if not time[1].isnumeric() or not time[2].isnumeric():
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "add <Day>:<Hour>:<Minute> <Title> <Desc> <Channel>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if len(time[1]) == 1:
            time[1] = "0" + time[1]

        if len(time[2]) == 1:
            time[2] = "0" + time[2]

        if not discord.utils.get(message.guild.text_channels, name=args[4]):
            await message.channel.send("**Channel not found.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        args[2] = args[2].replace("_", " ")
        args[3] = args[3].replace("_", " ")
        write_config(message.guild.id, args[1], args[2], args[3], args[4])
        await message.channel.send(
            embed=discord.Embed(
                title='Rule added.',
                description='**Title: **' + args[2] + """\n**Description:
                **""" + args[3] + "\n**Channel: **" + args[4] + "\n**Time: **" + args[1],
                color=0x0099ff
            )
        )
        await message.add_reaction(EMOJI_SUCCESS)
        return True

async def clean(message, args):
    """ Clean all calendar """
    if args[0] == "clean":
        if checkf(WORKPATH + "/configs/" + str(message.guild.id)):
            rmdir(WORKPATH + "/configs/" + str(message.guild.id))
            mkdir(WORKPATH + "/configs/" + str(message.guild.id))
            await message.channel.send("**Calendar is now empty.**")
            await message.add_reaction(EMOJI_OK)
            return True
        await message.channel.send("**Already empty.**")
        await message.add_reaction(EMOJI_FAILED)
        return True

async def remove(message, args):
    """ Remove rule from calendar """
    if args[0] == "remove":
        if not len(args) > 1:
            await message.channel.send("""**Missing arguments.\n
                Usage: `""" + PREFIX + "remove <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        time = args[1].split(":")
        if not len(time) == 3:
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "remove <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if not time[1].isnumeric() or not time[2].isnumeric():
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "remove <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if len(time[1]) == 1:
            time[1] = "0" + time[1]

        if len(time[2]) == 1:
            time[2] = "0" + time[2]

        remove_config(message.guild.id, args[1])
        await message.channel.send("**Rule removed.**")
        await message.add_reaction(EMOJI_SUCCESS)
        return True


async def change(message, args):
    """ Change rule in calendar """
    if args[0] == "change":
        if not len(args) > 3:
            await message.channel.send("""**Missing argument.\n
                Usage: `""" + PREFIX + "change <Day>:<Hour>:<Minute> <Config> <Data>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        time = args[1].split(":")
        if not len(time) == 3:
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "change <Day>:<Hour>:<Minute> <Config> <Data>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if not time[1].isnumeric() or not time[2].isnumeric():
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "change <Day>:<Hour>:<Minute> <Config> <Data>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        old_config = read_config(message.guild.id, args[1])
        if not old_config:
            await message.channel.send("**Rule not found. Use **`add`** instead.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if args[2] == "title":
            write_config(
                message.guild.id,
                args[1],
                args[3],
                old_config[1],
                old_config[2])
            await message.channel.send(
                embed=discord.Embed(
                    title='Success',
                    description="""**Configuration
                        in time **`""" + args[1] + "`** has been modified.**",
                    color=0x0099ff
                )
            )
            await message.add_reaction(EMOJI_SUCCESS)
        elif args[2] == "description":
            write_config(
                message.guild.id,
                args[1],
                old_config[0],
                args[3],
                old_config[2])
            await message.channel.send(
                embed=discord.Embed(
                    title='Success',
                    description="""**Configuration
                        in time **`""" + args[1] + "`** has been modified.**",
                    color=0x0099ff
                )
            )
            await message.add_reaction(EMOJI_SUCCESS)
        elif args[2] == "channel":
            write_config(
                message.guild.id,
                args[1],
                old_config[0],
                old_config[1],
                args[3])
            await message.channel.send(
                embed=discord.Embed(
                    title='Success',
                    description="""**Configuration
                        in time **`""" + args[1] + "`** has been modified.**",
                    color=0x0099ff
                )
            )
            await message.add_reaction(EMOJI_SUCCESS)
        else:
            await message.channel.send("""**Unknown config.\n
                Available configs: `title`, `channel`, `description`.**""")
            await message.add_reaction(EMOJI_FAILED)
        return True


async def disable(message, args):
    """ Disable rule in calendar """
    if args[0] == "disable":
        if not len(args) > 1:
            await message.channel.send("""**Missing argument.\n
                Usage: `""" + PREFIX + "disable <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        time = args[1].split(":")
        if not len(time) == 3:
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "disable <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if not time[1].isnumeric() or not time[2].isnumeric():
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "disable <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        mkfile(WORKPATH + "/configs/" + str(message.guild.id) +
               "/" + str(args[1]) + "/lock", "yep")
        await message.channel.send("**Disabled successfully.**")
        await message.add_reaction(EMOJI_SUCCESS)
        return True


async def enable(message, args):
    """ Enable rule in calendar """
    if args[0] == "enable":
        if not len(args) > 1:
            await message.channel.send("""**Missing argument.\n
                Usage: `""" + PREFIX + "enable <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        time = args[1].split(":")
        if not len(time) == 3:
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "enable <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if not time[1].isnumeric() or not time[2].isnumeric():
            await message.channel.send("""**Time is not correct.\n
                Usage: `""" + PREFIX + "enable <Day>:<Hour>:<Minute>`.**")
            await message.add_reaction(EMOJI_FAILED)
            return True

        if checkf(WORKPATH + "/configs/" + str(message.guild.id) +
                  "/" + str(args[1] + "/lock")):
            rmfile(WORKPATH +
                   "/configs/" +
                   str(message.guild.id) +
                   "/" +
                   str(args[1]) +
                   "/lock")
            await message.channel.send("**Enabled successfully.**")
            await message.add_reaction(EMOJI_OK)
            return True
        await message.channel.send("**Failed, already enabled.**")
        await message.add_reaction(EMOJI_FAILED)
        return True


async def extendedcommands(message, args):
    """ Some additional comands """
    if await disable(message, args):
        return True
    if await enable(message, args):
        return True
    if await change(message, args):
        return True
    if await clean(message, args):
        return True


async def botcommands(message, args):
    """ Main commands functions """

    if not message.author.guild_permissions:
        return True

    if not message.author.guild_permissions.manage_messages:
        return True

    if await add(message, args):
        return True
    if await remove(message, args):
        return True
    if await extendedcommands(message, args):
        return True


async def basictools(self, message):
    """ Some basic things to the bot, like devtools """
    if message.content == PREFIX + \
            'shutdown' and str(message.author) == HOSTUSER:
        await message.channel.send("**Stopping bot runtime..**")
        print(get_full_datetime() + "Stopping runtime...")
        if self.voice_clients:
            for _i in self.voice_clients:
                await _i.disconnect()
        await message.add_reaction(EMOJI_SUCCESS)
        await self.close()
        return True

    if message.content == PREFIX + "help":
        text = ":green_book: **Available commands (for everyone):**\n"
        for _command in help_all:
            text += "**`" + _command[0] + "`** - **" + _command[1] + "**\n"

        if not message.author.dm_channel:
            if message.author.guild_permissions:
                if message.author.guild_permissions.manage_messages:
                    text += ":loudspeaker: **For mods:**\n"
                    for _command in help_mod:
                        text += "**`" + _command[0] + \
                            "`** - **" + _command[1] + "**\n"

        if str(message.author) == HOSTUSER:
            text += ":cyclone: **For dev:**\n"
            for _command in help_dev:
                text += "**`" + _command[0] + "`** - **" + _command[1] + "**\n"

        await message.channel.send(
            embed=discord.Embed(title="**PREFIX **`" + PREFIX + "`.",
                                description=text,
                                color=0x6f0b91)
        )
        await message.add_reaction(EMOJI_OK)
        return True

bot = commands.Bot(command_prefix=PREFIX)

bot.last_time = ""


@bot.event
async def on_ready():
    """ Intialize loops """
    print(get_full_datetime() + 'Logged as: ', bot.user)
    bot.loop.create_task(fix_status(bot))
    bot.loop.create_task(calendar_process(bot))


@bot.event
async def on_message(message):
    """ Main message event """
    if message.author == bot.user or not message.guild:
        return  # Don't reply on itself

    if(message.author and message.channel):  # check if message is from DM
        if message.author.dm_channel:
            if message.channel.id == message.author.dm_channel.id:
                return

    tick = 0
    if len(message.content) > len(PREFIX):  # Check message contains PREFIX
        for _letter in PREFIX:
            if not PREFIX[tick] == message.content[tick]:
                return
            tick += 1
    else:
        return

    if await basictools(bot, message):
        return

    args = decontruct_string(message.content)
    args[0] = args[0][len(PREFIX):]

    if await show_calendar(message, args):
        return

    if not await botcommands(message, args):
        await message.channel.send(
            """**Command not found.
            Check** `""" + PREFIX + """help` **for
            available commands.**"""
        )
        await message.add_reaction(EMOJI_FAILED)


async def fix_status(self):
    """ Show status """
    run = True
    while run:
        await asyncio.sleep(10)
        for voicechannel in self.voice_clients:
            if voicechannel.is_connected():
                voicechannel.disconnect()
        await self.change_presence(
            activity=discord.Game(name="" + PREFIX + "help"),
            status=discord.Status.online
        )
        run = False


async def calendar_process(self):
    """ It sends a message on a specific channel on a specific servers """
    while True:
        await asyncio.sleep(round(10 / len(self.guilds)))
        datetime_object = datetime.datetime.now()
        delta_time = datetime_object.hour + int(TIMEZONE)
        if delta_time > 23:
            delta_time = delta_time - 24
        datetime_object = datetime_object.replace(hour=delta_time)
        delta_minute = str(datetime_object.minute + 3)
        if len(delta_minute) == 1:
            delta_minute = "0" + delta_minute
        delta_hour = str(datetime_object.hour)
        if len(delta_hour) == 1:
            delta_hour = "0" + delta_hour
        now_time = str(datetime.datetime.now().strftime("%A")) + \
            ":" + delta_hour + ":" + delta_minute
        if self.last_time != now_time:
            self.last_time = now_time
            for guild_id in os.listdir(WORKPATH + "/configs"):
                if checkf(WORKPATH + "/configs/" + guild_id + "/" + now_time):
                    if not checkf(
                        WORKPATH +
                        "/configs/" +
                        guild_id +
                        "/" +
                        now_time +
                            "/lock"):
                        data = read_config(guild_id, now_time)
                        guild = discord.utils.get(
                            self.guilds, id=int(guild_id))
                        channel = discord.utils.get(
                            guild.text_channels, name=data[2])
                        await channel.send(
                            embed=discord.Embed(
                                title=data[0],
                                description=data[1],
                                color=0x0099ff
                            )
                        )

help_all = [
    ["help", "Show help."],
    ["calendar", "Show server calendar."]
]

help_dev = [
    ["shutdown", "Stop bot runtime."],
    ["prefix <prefix>", "Change main PREFIX."]
]

help_mod = [["add <time> <title> <description> <channel>",
             "Add rule to the calendar. (Use _ to expand title etc.)"],
            ["remove <time>",
             "Remove rule from calendar."],
            ["change <time> <type={channel,title,description}> <content>",
             "Modify rule."],
            ["disable <time>",
             "Disable rule."],
            ["enable <time>",
             "Enable rule."],
            ["clean",
             "Clean calendar."]]

# Start bot
if __name__ == "__main__":
    while True:
        bot.run(TOKEN)
